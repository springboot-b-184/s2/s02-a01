package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

    @Entity
//name the table
    @Table(name="users")
    public class User {
        //indicate the primary key
        @Id
        //auto-increment the ID column
        @GeneratedValue
        private long id;
        @Column
        private String username;

        @Column
        private String password;

        //default constructor needed when retrieving posts
        public User(){}

        //other necessary constructors
        public User(String username,String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
